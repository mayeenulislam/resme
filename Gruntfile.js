/**
 * Resme Gruntfile Directives
 *
 * @package     Resme
 * @version     1.0.0
 */

module.exports = function(grunt) {

    // @Grunt: Get our configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /**
         * Validate files with JSHint
         * @author: https://github.com/gruntjs/grunt-contrib-jshint
         */
        jshint: {
            all: ['Gruntfile.js', 'assets/js/resme.js']
        },

        /**
         * Concatenate & Minify Javascript files
         * @author: https://github.com/gruntjs/grunt-contrib-uglify
         */
        uglify: {
            public: {
                options: {
                    sourceMap: true
                },
                files: {
                    'assets/js/plugins.js': [
                                                'node_modules/placeholder/dist/placeholder.js', //v1.0.2
                                                'node_modules/dist/fitvids.js', //v2.0.0
                                                'node_modules/waypoints/lib/noframework.waypoints.js', //v4.0.1
                                                'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js', //v3.3.7
                                                'node_modules/jquery-match-height/jquery.matchHeight.js', //v0.7.0
                                                'node_modules/jquery-countto/jquery.countTo.js', //v1.2.0
                                            ],
                    'assets/js/resme.min.js': [ 'assets/js/resme.js' ]
                },
            }
        },

        /**
         * Compile SCSS files into CSS
         * @author: https://github.com/sindresorhus/grunt-sass/
         */
        sass: {
            dist: {
                options: {
                    sourceMap: true
                },
                files: {
                    'assets/css/resme.css': 'assets/sass/resme.scss'
                }
            }
        },

        /**
         * Add vendor prefixes
         * @author: https://github.com/nDmitry/grunt-autoprefixer
         */
        autoprefixer: {
            options: {
                cascade: false
            },
            resmeCSS: {
                src: 'assets/css/resme.css'
            }
        },


        /**
         * Minify Stylehseets for production
         * @author: https://github.com/gruntjs/grunt-contrib-cssmin
         */
        cssmin: {
            minify: {
                files: {
                    'assets/css/resme.css': 'assets/css/resme.css',
                },
                options: {
                    report: 'min',
                    keepSpecialComments: 0
                }
            }
        },


        /**
         * Watch for changes and do it
         * @author: https://github.com/gruntjs/grunt-contrib-watch
         */
        watch: {
            options: {
                livereload: {
                    port: 9000
                }
            },
            js: {
                files: ['assets/js/resme.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['assets/sass/*.scss'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            }
        }

    });


    // @Grunt: we're using the following plugins
    require('load-grunt-tasks')(grunt);


    // @Grunt: do the following when we will type 'grunt'
    grunt.registerTask('default', ['jshint', 'uglify', 'sass', 'autoprefixer', 'cssmin']);

};
